# land-of-lisp

A port of the clsip code for Land Of Lisp into Clojure. The goal is to learn clofure by getting the code to run in clojure.

## Installation

Download from https://bitbucket.org/hippeelee/land-of-lisp-for-clojure

## Usage

    $ java -jar land-of-lisp-0.1.0-standalone.jar [args]

## Options

FIXME: listing of options this app accepts.

## Examples

N/A

### Bugs

N/A

## License

Copynone © 2014

Nothing claimed and there are no gurantees. Use at your worn discretion and risk.
